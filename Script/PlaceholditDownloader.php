<?php

    /**
     * @file PlaceholdiDownloader.php
     *
     * Downloads placeholder images from the placehold.it service.
     *
     * @author Jimmy K. <jimmy@orbitmedia.com>
     * @version 1.0.0
     */

    class PlaceholditDownloader {

        private $sizes = [];
        private $colors = [];

        /**
         * Add a size to the size array.
         *
         * @return void
         * @author Jimmy K. <jimmy@orbitmedia.com>
         * @since 1.0.0
         */

        public function addSize($width, $height)
        {

            $this->sizes[] = [
                'width' => $width,
                'height' => $height,
            ];

        }

        /**
         * Get the sizes array.
         *
         * @return array
         * @author Jimmy K. <jimmy@orbitmedia.com>
         * @since 1.0.0
         */

        public function getSizes()
        {

            return $this->sizes;

        }

        /**
         * Add a color to the colors array.
         *
         * @return void
         * @author Jimmy K. <jimmy@orbitmedia.com>
         * @since 1.0.0
         */

        public function addColor($name, $background, $foreground)
        {

            $this->colors[] = [
                'name' => $name,
                'background' => $background,
                'foreground' => $foreground,
            ];

        }

        /**
         * Get the colors array.
         *
         * @return array
         * @author Jimmy K. <jimmy@orbitmedia.com>
         * @since 1.0.0
         */

        public function getColors()
        {

            return $this->colors;

        }

        /**
         * Download the image at the specified size and color index.
         *
         * @return void
         * @author Jimmy K. <jimmy@orbitmedia.com>
         * @since 1.0.0
         */

        public function downloadImage($sizeIndex, $colorIndex, $destination)
        {

            // Get the size.
            $size = $this->sizes[$sizeIndex]['width'] . 'x' . $this->sizes[$sizeIndex]['height'];

            // Get the background color.
            $background = $this->colors[$colorIndex]['background'];

            // Get the foreground color.
            $foreground = $this->colors[$colorIndex]['foreground'];

            // Determine the remote path.
            $remote = 'http://placehold.it/' . $size . '/' . $background . '/' . $foreground;
            // echo $remote . '<br />';

            // Determine the local path.
            $local = $destination . $size . '/' . $size . '-' . $this->colors[$colorIndex]['name'] . '.gif';
            // echo $local . '<br />';

            if (!is_dir($destination . $size)) {
                // Try to make the local directory if it doesn't exist.
                mkdir($destination . $size);
            }

            // Fetch the image.
            $response = file_get_contents($remote) or die('Unable to fetch image.');
            // echo $response . '<br />';

            // Save the image.
            file_put_contents($local, $response) or die('Unable to save image.');

        }

    }
