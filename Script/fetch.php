<?php

    /**
     * @file fetch.php
     *
     * This script will fetch all of the required images from the Placehold.it
     * service. To avoid abusing the Placehold.it service, this script will reload
     * itself every X seconds until it has downloaded all of the images.
     *
     * @author Jimmy K. <jimmy@ghazlawl.com>
     * @version 1.0.0
     */

    // Load the downloader class.
    require('PlaceholditDownloader.php');

    // Create a new downloader.
    $downloader = new PlaceholditDownloader();

    /* ======================================== */
    /* Sizes
    /* ======================================== */

    $downloader->addSize(1500, 500); // Banner
    $downloader->addSize(600, 400); // Portrait
    $downloader->addSize(600, 800); // Landscape
    $downloader->addSize(600, 600); // Square

    /* ======================================== */
    /* Colors
    /* ======================================== */

    $downloader->addColor('Blue', 'ccccff', 'b7b7e5');
    $downloader->addColor('Cyan', 'ccffff', 'b7e5e5');
    $downloader->addColor('Gray', 'dddddd', 'c6c6c6');
    $downloader->addColor('Green', 'ccffcc', 'b7e5b7');
    $downloader->addColor('Purple', 'ffddff', 'e5c6e5');
    $downloader->addColor('Red', 'ffdddd', 'e5c6c6');
    $downloader->addColor('White', 'ffffff', 'e5e5e5');
    $downloader->addColor('Yellow', 'ffffcc', 'e5e5b7');

    /* ======================================== */
    /* Loop Magic
    /* ======================================== */

    // Get the colors.
    $colors = $downloader->getColors();

    $index = isset($_GET['color']) ? $_GET['color'] : 0;

    if ($index + 1 < count($colors)) {
        // Move to the next color.
        $reloadURL = './fetch.php?color=' . ($index + 1);
    }

    /* ======================================== */
    /* Download
    /* ======================================== */

    // Get the sizes.
    $sizes = $downloader->getSizes();

    foreach ($sizes as $k => $v) {
        // Download each size of the current color (based on the index).
        $downloader->downloadImage($k, $index, '../');
    }

    /* ======================================== */
    /* Output
    /* ======================================== */

    ?>

    <html>
    <head>
    <title>Placehold.it Image Downloader</title>

        <?php if ($reloadURL) : ?>
            <script>
                setTimeout(function() {
                    window.location.href = '<?php echo $reloadURL; ?>';
                }, 5000);
            </script>
        <?php endif; ?>

    </head>
    <body>

        <?php if ($index + 1 == count($colors)) : ?>
            All color sets downloaded.
        <?php else : ?>
            Downloading color set <?php echo ($index + 1); ?> of <?php echo count($colors); ?>.
        <?php endif; ?>

    </body>
    </html>
