A collection of placeholder images andin various colors, saved at preferred
dimensions, using the Placehold.it image service. Also includes avatars!

# Image Sizes #

Image sizes are as follows:

* Banner `1500x500`
* Portrait `600x800`
* Landscape `600x400`
* Square `600x600`

# Photoshop Files #

There are Photoshop files for the following items:

* Color Palette
* Avatars

# Images Preview #

!["Blue"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Blue.gif)

!["Cyan"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Cyan.gif)

!["Gray"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Gray.gif)

!["Green"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Green.gif)

!["Purple"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Purple.gif)

!["Red"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Red.gif)

!["White"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-White.gif)

!["Yellow"](https://bytebucket.org/ghazlawl/placeholder-images/raw/5d4a8e138bdb1a343cd60ea602280614ba1f720b/600x400/600x400-Yellow.gif)
